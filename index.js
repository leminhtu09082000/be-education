const express = require("express");
const authRouter = require("./src/routes/auth");
const postRouter = require("./src/routes/post");
const connectDB = require("./src/configs/db.config");
const cors = require("cors");
require("dotenv").config();
const app = express();

//connect to db
connectDB();
app.use(express.json());
app.use(cors());
app.use("/api/auth", authRouter);
app.use("/api/posts", postRouter);

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`Server started on ${PORT}`));
