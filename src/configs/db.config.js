const mongoose = require("mongoose");
const connectDB = async () => {
  try {
    await mongoose.connect(
      `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@education.5mefh.mongodb.net/education?retryWrites=true&w=majority`
    );
    console.log("MongoDB connect successfuly");
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
};
module.exports = connectDB;
